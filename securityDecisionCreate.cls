 Assessment__c assessmentRec = AssesmentWrapper.getAssessment(assessmentObj);
 List<Assessment__c> assessList = new List<Assessment__c>();
 assessList.add(assessmentRec);
 SObjectAccessDecision asseSecurityDecisionCreate = Security.stripInaccessible( AccessType.CREATABLE, assessList);
 insert asseSecurityDecisionCreate.getRecords();